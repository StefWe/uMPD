/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playerdatabase.h"

PlayerDatabase::PlayerDatabase(QObject *parent) : Connect(parent)
{
  auto dirmodel = new PlayListModel(this);
  setDirmodel(dirmodel);
  auto navmodel = new PlayListModel(this);
  setNavmodel(navmodel);
  
  connect(this, SIGNAL(connected()), this, SLOT(onConnected()));
}

void PlayerDatabase::setDirmodel(PlayListModel* dirmodel)
{
  m_dirmodel = dirmodel;
}

PlayListModel * PlayerDatabase::dirmodel()
{
  return m_dirmodel;
}

void PlayerDatabase::setNavmodel(PlayListModel* navmodel)
{
  m_navmodel = navmodel;
}

PlayListModel * PlayerDatabase::navmodel()
{
  return m_navmodel;
}

void PlayerDatabase::onConnected()
{
    getActDir("");
}

bool PlayerDatabase::update()
{
  unsigned int retVal;
  retVal = mpd_run_update(m_connection, NULL);
  
  if( retVal != 0 ) {
    return true;
  } else {
    return false;
  }
}

void PlayerDatabase::setNavpos(int navpos)
{
  if (navpos != m_navpos) {
    m_navpos = navpos;
    qDebug() << "Navpos:" << m_navpos;
    emit navposChanged(m_navpos);
  }
}

int PlayerDatabase::navPos() const
{
  return m_navpos;
}

QString PlayerDatabase::getLastFolder(QString path)
{
  if (path.lastIndexOf("/") != -1) {
    QStringRef test = path.rightRef(path.size() - path.lastIndexOf("/") - 1);
    return test.toString();
  } else {
    return path;
  }
}

void PlayerDatabase::getActDir(QString directory)
{
  
  m_navmodel->loadNavList(getLastFolder(directory), directory);
  setNavpos(navPos() + 1);
  
  qDebug() << "Abfrage aktuelles Verzeichnisse: " << mpd_send_list_meta(m_connection, utils::QStringToBA(directory).data());
  const struct mpd_directory *dir;
  
  QStringList dirList;
  QStringList dirTitle;
  QStringList entryTyp;
  
  struct mpd_entity *entity;
  const struct mpd_song *song;

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_DIRECTORY)
    {
      dir = mpd_entity_get_directory(entity);
      dirList << mpd_directory_get_path(dir);
      dirTitle << getLastFolder(mpd_directory_get_path(dir));
      entryTyp << "0"; // 0 = directory
    }
    else if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG)
    {
      song = mpd_entity_get_song(entity);
      dirList <<  mpd_song_get_uri(song);
      dirTitle << mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
      entryTyp << "1"; // 1 = song
    }
    
    mpd_entity_free(entity);
    
  }

  m_dirmodel->loadDirlists(entryTyp, dirTitle, dirList);
  
}

void PlayerDatabase::getActDir(int position, QString directory)
{
  setNavpos(position);
  m_navmodel->removeNavList(position, directory);
  
  qDebug() << "Abfrage aktuelles Verzeichnisse: " << mpd_send_list_meta(m_connection, utils::QStringToBA(directory).data());
  const struct mpd_directory *dir;
  
  QStringList dirList;
  QStringList dirTitle;
  QStringList entryTyp;
  
  struct mpd_entity *entity;
  const struct mpd_song *song;

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_DIRECTORY)
    {
      dir = mpd_entity_get_directory(entity);
      dirList << mpd_directory_get_path(dir);
      dirTitle << getLastFolder(mpd_directory_get_path(dir));
      entryTyp << "0"; // 0 = directory
    }
    else if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG)
    {
      song = mpd_entity_get_song(entity);
      dirList << mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
      dirTitle << mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
      entryTyp << "1"; // 1 = song
    }
    
    mpd_entity_free(entity);
  
  }
  
  m_dirmodel->loadDirlists(entryTyp, dirTitle, dirList);
  
}
