/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYLIST_H
#define PLAYLIST_H

#include "utils.h"
#include "playlistmodel.h"
#include "connect.h"

class PlayList : public Connect
{
  Q_OBJECT
  
  Q_PROPERTY(PlayListModel *playmodel READ playmodel WRITE setPlaymodel NOTIFY playmodelChanged)
  Q_PROPERTY(int selPlaylist READ selPlaylist WRITE setPlaylist NOTIFY selPlaylistChanged)
  Q_PROPERTY(QString name READ name WRITE setName NOTIFY updateName);
  
public:
  PlayList(QObject *parent = 0);
  
  Q_INVOKABLE void checkPlaylists(void);
  Q_INVOKABLE bool saveAs(QString playlistName);
  Q_INVOKABLE bool addToQueue(QString name);
  Q_INVOKABLE bool remove(QString name);
  Q_INVOKABLE void selectedEntity(QString playlistName, QString playlistNumber);
  Q_INVOKABLE void removeSong(QString playlistName, unsigned pos);

  void getContent(QString playlist);
  
  void setPlaylist(int playlistNum);
  int selPlaylist() const;
  
  void setName(QString name);
  QString name() const;
  
  PlayListModel *playmodel();
  void setPlaymodel(PlayListModel *playmodel);
  
signals:
  void playmodelChanged(PlayListModel *playmodel);
  void selPlaylistChanged(int playlist);
  void updateName(QString name);

private slots:
  void onConnected();
  
private:
  QString m_name = "";
  int m_playlistNum = 0;

  PlayListModel *m_playmodel = nullptr;
};

#endif
